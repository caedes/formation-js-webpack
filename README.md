# Formation JavaScript Webpack

## Getting Started

```shell
npm install
npm run build
```

## Run Server

```shell
npm start
```

On Chrome, or any other browser go to: http://localhost:3000/
