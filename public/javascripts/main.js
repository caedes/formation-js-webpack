import addedProductNodes from './helpers/added-product-nodes';
import calculateTotalPrice from './helpers/calculate-total-price';
import jhr from './helpers/jhr';
import productNode from './helpers/product-node';

document.addEventListener('DOMContentLoaded', () => {
  const itemNodes = document.querySelectorAll('.col-sm.item');
  const totalTable = document.querySelector('#total tbody');

  const removeProductToCart = (event) => {
    event.preventDefault();
    const { skuToRemove } = event.currentTarget.dataset;
    totalTable.querySelector(`[data-sku=${skuToRemove}]`).remove();

    calculateTotalPrice();
  };

  const addProductToCart = (event) => {
    event.preventDefault();
    const button = event.currentTarget;
    totalTable.innerHTML = addedProductNodes(button.dataset) + totalTable.innerHTML;

    calculateTotalPrice();

    const linkNodeList = totalTable.querySelectorAll('tr.product a');
    const linkNodes = Array.prototype.slice.call(linkNodeList);
    linkNodes.forEach((link) => {
      link.addEventListener('click', removeProductToCart);
    });
  };

  jhr('GET', '/collection', (event) => {
    const response = JSON.parse(event.currentTarget.responseText);
    if (response.status === 200) {
      const products = response.data;

      products.forEach((product, i) => {
        const itemNode = itemNodes[i];
        itemNode.innerHTML = productNode(product);
        itemNode.querySelector('button').addEventListener('click', addProductToCart);
      });
    }
  });

  calculateTotalPrice();
});
