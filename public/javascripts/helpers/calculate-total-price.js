import formatPrice from './format-price';

export default () => {
  const totalTable = document.querySelector('#total tbody');

  const addedProductPriceNodesList = totalTable.querySelectorAll('.product > td:last-child');
  const addedProductPriceNodes = Array.prototype.slice.call(addedProductPriceNodesList);
  const addedProductPrices = addedProductPriceNodes.map(addedProductPriceNode =>
    parseFloat(addedProductPriceNode.innerHTML.replace('€', '')));
  const totalPrice = addedProductPrices.reduce((i, j) => i + j, 0);

  const totalHTNode = totalTable.querySelector('#total-ht');
  totalHTNode.innerHTML = `${formatPrice(totalPrice)}€`;

  const totalTTCPrice = 1.2 * totalPrice;
  const totalTTCNode = totalTable.querySelector('#total-ttc');
  totalTTCNode.innerHTML = `${formatPrice(totalTTCPrice)}€`;
};
