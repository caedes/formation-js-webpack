export default (verb, url, callback) => {
  const jhr = new XMLHttpRequest();
  jhr.open(verb, url, false);
  jhr.setRequestHeader('Content-Type', 'application/json');
  jhr.onload = callback;
  jhr.send();
};
