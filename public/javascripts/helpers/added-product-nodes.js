import formatPrice from './format-price';

export default (product) => {
  const { sku, name, price } = product;

  return `<tr class="product" data-sku="${sku}">
    <td><a role="button" data-sku-to-remove="${sku}" class="remove">❌</a></td>
    <th scope="row">${sku}</th>
    <td>${name}</td>
    <td>${formatPrice(price)}€</td>
  </tr>`;
};
