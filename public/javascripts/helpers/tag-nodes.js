export default tags => tags.map(tag => `<span class="badge badge-dark">${tag}</span>`);
