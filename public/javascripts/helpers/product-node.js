import formatPrice from './format-price';
import tagNodes from './tag-nodes';

export default (product) => {
  const {
    sku, url, name, description, price, currency, composition,
  } = product;

  return `<img src="${url}" width="200" height="200" />
  <hr>
  <h3>${name}</h3>
  <p>${description}</p>
  <hr>
  <span>${formatPrice(price)}${currency}</span>
  <br />
  <div class="tags">${tagNodes(composition)}</div>
  <button
    class="btn btn-primary"
    type="submit"
    data-sku="${sku}"
    data-name="${name}"
    data-price="${price}"
  >
    Ajouter au panier
  </button>`;
};
